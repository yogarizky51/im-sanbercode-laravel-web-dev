<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class CastController extends Controller
{
    public function index()
    {
        $cast = DB::table('casts')->get();
        // dd($cast);
        return view('cast.list-cast', ['cast' => $cast]);
    }

    public function create()
    {
        return view('cast.create-cast');
    }

    public function store(Request $request)
    {
        $request->validate([
            'nama' => 'required',
            'umur' => 'required',
            'bio' => 'required',
        ]);
        DB::table('casts')->insert([
            "nama" => $request["nama"],
            "umur" => $request["umur"],
            "bio" => $request["bio"]
        ]);
        return redirect('/cast');
    }

    public function show($id)
    {
        $cast = DB::table('casts')->find($id);
        return view('cast.detail-cast', ['cast' => $cast]);
    }
    public function edit($id)
    {
        $cast = DB::table('casts')->where('id', $id)->first();
        return view('cast.edit-cast', compact('cast'));
    }

    public function update($id, Request $request)
    {
        $request->validate([
            'nama' => 'required',
            'umur' => 'required',
            'bio' => 'required',
        ]);

        DB::table('casts')
            ->where('id', $id)
            ->update([
                "nama" => $request["nama"],
                "umur" => $request["umur"],
                "bio" => $request["bio"]
            ]);
        return redirect('/cast');
    }
    public function destroy($id)
    {
        DB::table('casts')->where('id', $id)->delete();
        return redirect('/cast');
    }
}
