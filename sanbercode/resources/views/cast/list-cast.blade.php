@extends('layouts.master')

@section('title', 'List Cast')

@section('content')
    <a href="/cast/create" class="btn btn-primary mb-2">Tambah Cast</a>
    <table class="table">
        <thead>
            <tr>
                <th scope="col">No</th>
                <th scope="col">Nama</th>
                <th scope="col">Umur</th>
                <th scope="col">Action</th>
            </tr>
        </thead>
        <tbody>
            @forelse ($cast as $key => $items)
                <tr>
                    <th scope="row">{{ $key + 1 }}</th>
                    <td>{{ $items->nama }}</td>
                    <td>{{ $items->umur }}</td>
                    <td>
                        <form action="/cast/{{ $items->id }}" method="post">
                            @method('delete')
                            @csrf
                            <a href="/cast/{{ $items->id }}" class="btn btn-info">Detail</a>
                            <a href="/cast/{{ $items->id }}/edit" class="btn btn-warning">Edit</a>
                            <input type="submit" class="btn btn-danger" value="Delete">
                        </form>
                    </td>
                </tr>
            @empty
                <td>Tidak ada data</td>
            @endforelse
        </tbody>
    </table>
@endsection
