@extends('layouts.master')

@section('title', 'Detail Cast')

@section('content')
    <h2>{{ $cast->nama }}</h2>
    <h4>{{ $cast->umur }}</h4>
    <p>{{ $cast->bio }}</p>

    <a href="/cast" class="btn btn-info">Kembali</a>
@endsection
