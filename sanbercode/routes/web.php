<?php

use App\Http\Controllers\AuthController;
use App\Http\Controllers\CastController;
use App\Http\Controllers\HomeController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', [HomeController::class, 'index']);
Route::get('/register', [AuthController::class, 'register']);
Route::post('/welcome', [AuthController::class, 'welcome']);
Route::get('/table', function () {
    return view('table');
});
Route::get('/data-tables', function () {
    return view('datatable');
});

//CRUD Cast
//List cast page
Route::get('/cast', [CastController::class, 'index']);
//Create cast page
Route::get('/cast/create', [CastController::class, 'create']);
//Create data cast
Route::post('/cast', [CastController::class, 'store']);
//Show data via id
Route::get('/cast/{cast_id}', [CastController::class, 'show']);
//get data edit via id
Route::get('/cast/{cast_id}/edit', [CastController::class, 'edit']);
//edit data
Route::put('/cast/{cast_id}', [CastController::class, 'update']);
//delete data
Route::delete('/cast/{cast_id}', [CastController::class, 'destroy']);
