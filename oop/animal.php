<?php

class Animal
{
    public $name;
    public $legs = 4;
    public $cold_blooded = "No";
    function __construct($name)
    {
        $this->name = $name;
    }
}
